const request = require("supertest");
const appCreator = require("../index.js");
const fs = require("fs");
const fetch = require("node-fetch");

let validatorId = -1;

jest.setTimeout(50000);
let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create validator', done => {
    request(app).post("/validators").send({
        name: "Validator test",
        description: "Description",
        columns: [
            "Test1",
        ]
    }).expect(200).then(res => {
        validatorId = res.body.id;
        done();
    });
});

let pipelineId = -1;
let clientId = -1;
let scheduleId = -1;
let timePeriods = [];

test('Test create mapping', async done => {
    pipelineId = await request(app).post("/pipelines").send({
        name: "test_pipeline",
    }).expect(200).then(pip => {
        return pip.body.id;
    });

    await request(app).post("/mappings").send({
        signature: "LOL",
        id: "e2e-success-test-mapping-2",
        validator: validatorId
    }).expect(200).then(pip => {
        return pip.body.id;
    });

    await request(app).post("/pipelines/" + pipelineId + "/data-source").send({
        id: "e2e-success-amazon-data-2",
        name: "Amazon data",
        type: "upload"
    }).expect(200);

    await request(app).post("/pipelines/" + pipelineId + "/mapping").send({
        mapping: "e2e-success-test-mapping-2",
        dataSource: "e2e-success-amazon-data-2"
    }).expect(200);

    clientId = await request(app).post("/clients").send({
        name: "Test client"
    }).expect(200).then(res => res.body.id);

    scheduleId = await request(app).post("/schedules").send({
        name: "SCHEDULE TEST",
        start: "2019-01-01",
        repeatEvery: 6,
        repeatPeriod: "month",
        recurring: true
    }).expect(200).then(res => res.body.id);

    timePeriods = await request(app).post("/clients/" + clientId + "/pipelines").send({
        pipeline: pipelineId,
        schedule: scheduleId
    }).expect(200).then(res => res.body);

    done();
});

let fileName = "";

test('Test upload file', async done => {
    fileName = encodeURIComponent("clients/" + clientId + "/uploaded/" + timePeriods[0].id + "/e2e-success-amazon-data-2/test-data.csv");

    let signedUrl = await request(app).post("/files/" + fileName).send().expect(200).then(res => res.text);

    let res = await fetch(signedUrl, {
        method: "PUT",
        body: fs.readFileSync("./test-data/csv-format.csv")
    });
    expect(res.status).toEqual(200);

    done();
});

test('Test wait for job', async done => {
    let jobs = [];

    while (jobs.length == 0) {
        jobs = await request(app).get("/jobs?file-name=" + fileName).expect(200).then(res => res.body);
    }

    expect(jobs.length).toBeGreaterThan(0);

    let job = jobs[0];
    let jobGroup = await request(app).get("/job-groups/" + job.job_group_id).expect(200).then(res => res.body);

    while (jobGroup.status != "DONE") {
        jobGroup = await request(app).get("/job-groups/" + job.job_group_id).expect(200).then(res => res.body);

        //console.log(job);
    }

    done();
});

let jobs = -1;

test('Test cleanup', async done => {
    await request(app).delete("/mappings/e2e-success-test-mapping-2").expect(200);
    await request(app).delete("/pipelines/" + pipelineId).expect(200);
    await request(app).delete("/schedules/" + scheduleId).expect(200);
    await request(app).delete("/clients/" + clientId).expect(200);
    await request(app).delete("/validators/" + validatorId).expect(200);

    //await request(app).delete("/jobs/" + jobs.id).expect(200);

    done();
});

afterAll(async () => {
    await app.close();
})
