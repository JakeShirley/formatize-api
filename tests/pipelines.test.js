const request = require("supertest");
const appCreator = require("../index.js");

let pipelineId = -1;
let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create pipeline', done => {
    request(app).post("/pipelines").send({
        name: "SimplyVAT"
    }).expect(200).then(res => {
        pipelineId = res.body.id;
        done();
    }).catch(err => {
        console.log(err);
    });
});

let mappingId = -1;

test('Test create mapping', done => {
    request(app).post("/mappings").send({
        id: "pipeline-test-mapping-1",
        name: "Mapping 1",
    }).expect(200).then(res => {
        mappingId = res.body.id;
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test create data source', done => {
    request(app).post("/pipelines/" + pipelineId + "/data-source").send({
        id: "amazon",
        name: "Amazon",
        type: "upload"
    }).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test list mapping', done => {
    request(app).post("/pipelines/" + pipelineId + "/mapping").send({
        mapping: mappingId,
        dataSource: "amazon"
    }).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test get pipeline', done => {
    request(app).get("/pipelines/" + pipelineId).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test delete pipeline', done => {
    request(app).delete("/pipelines/" + pipelineId).expect(200).then(res => {
        done();
    });
});


test('Test delete mapping', done => {
    request(app).delete("/mappings/" + mappingId).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

afterAll(async () => {
    await app.close();
})
