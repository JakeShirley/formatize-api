const request = require("supertest");
const appCreator = require("../index.js");


let apiKeyId = -1;
let apiKey = "";
let token = "";
let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create api-keys', done => {
    request(app).post("/api-keys").send().expect(200).then(res => {
        apiKeyId = res.body.id;
        apiKey = res.body.api_key;
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test api-key with client creation', done => {
    request(app).post("/clients").set("Authorization", "API-Key " + apiKey).send({
        name: "Test client"
    }).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test delete api-key', done => {
    request(app).delete("/api-keys/" + apiKeyId).set("Authorization", "API-Key " + apiKey).expect(200).then(res => {
        done();
    });
});

afterAll(async () => {
    await app.close();
})
