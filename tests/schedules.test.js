const request = require("supertest");
const appCreator = require("../index.js");

jest.setTimeout(25000);

let scheduleId = -1;
let clientId = -1;
let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create schedule', done => {
    request(app).post("/schedules").send({
        name: "SCHEDULE TEST",
        start: "2019-12-01",
        //end: "2019-04-01",
        repeatEvery: 5,
        repeatPeriod: "day",
        duration: 5,
        recurring: true,
    }).expect(200).then(res => {
        scheduleId = res.body.id;
        done();
    });
});

test('Test create time periods', async done => {
    clientId = await request(app).post("/clients").send({
        name: "Test client"
    }).expect(200).then(res => {
        return res.body.id;
    });

    let pipelineId = await request(app).post("/pipelines").send({
        name: "Test client"
    }).expect(200).then(res => {
        return res.body.id;
    });

    let timePeriods = await request(app).post("/clients/" + clientId + "/pipelines").send({
        pipeline: pipelineId,
        schedule: scheduleId
    }).expect(200).then(res => {
        return res.body;
    });

    timePeriods = await request(app).post("/schedules/update").send({
        client: clientId,
        schedule: scheduleId
    }).expect(200).then(res => {
        return res.body;
    });

    await request(app).delete("/pipelines/" + pipelineId).expect(200);

    await request(app).delete("/clients/" + clientId).expect(200);

    done();
});

test('Test delete schedule', done => {
    request(app).delete("/schedules/" + scheduleId).expect(200).then(() => done());
});

afterAll(async () => {
    await app.close();
})
