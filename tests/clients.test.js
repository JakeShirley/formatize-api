const request = require("supertest");
const appCreator = require("../index.js");

jest.setTimeout(5000);

let token = "";
let clientId = -1;
let pipelineId = -1;
let scheduleId = -1;
let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create client', done => {
    request(app).post("/clients").send({
        name: "SimplyVAT",
        status: "TESTING"
    }).expect(200).then(res => {
        clientId = res.body.id;
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test create pipeline', done => {
    request(app).post("/pipelines").send({
        name: "SimplyVAT"
    }).expect(200).then(res => {
        pipelineId = res.body.id;
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test create schedule', done => {
    request(app).post("/schedules").send({
        name: "SimplyVAT",
        start: "2020-01-01",
        duration: 28,
        repeatEvery: 1,
        repeatPeriod: "week",
        recurring: true,
    }).expect(200).then(res => {
        scheduleId = res.body.id;
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test add pipeline to load', done => {
    request(app).post("/clients/" + clientId + "/pipelines").send({
        pipeline: pipelineId,
        schedule: scheduleId
    }).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test get client', done => {
    request(app).get("/clients/" + clientId).expect(200).then(res => {
        done();
    }).catch(err => {
        console.log(err);
    });
});

test('Test delete client', done => {
    request(app).delete("/clients/" + clientId).expect(200).then(res => {
        done();
    });
});

test('Test delete pipeline', done => {
    request(app).delete("/pipelines/" + pipelineId).expect(200).then(res => {
        done();
    });
});

afterAll(async () => {
    await app.close();
})
