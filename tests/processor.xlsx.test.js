const { XLSXReader, outputToString, processRow } = require("../common/processor.js");

const request = require("supertest");

const AWS = require('aws-sdk/global');
const S3 = require('aws-sdk/clients/s3');
const SQS = require('aws-sdk/clients/sqs');

const fs = require("fs");

AWS.config.region = 'eu-west-1';

const BUCKET_NAME = process.env.BUCKET_NAME;
const transforms = require("../common/transform-functions");

let s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: BUCKET_NAME }
});

let reader = new XLSXReader(s3);

jest.setTimeout(120000);

const fileName = "test-data.xlsx";

test('Test upload file', async done => {

    await s3.upload({
        Key: fileName,
        Body: fs.readFileSync("./test-data/excel-format.xlsx")
    }).promise();

    done();
});

test("Test get byte ranges process", done => {
    reader.getPartByteRanges(fileName, 64).then(parts => {
        expect(parts).toHaveLength(1);

        done();
    });
});

test("Test big data process", done => {
    reader.getPartByteRanges(fileName, 10).then(parts => {
        reader.getHeaders(fileName).then(async oHeaders => {
            let headers = {};
            let info = {
                headers, 
                transforms: [
                    transforms["keep-column"].func({ column: 1 }, { oHeaders, headers }),
                    transforms["rename-column"].func({ src: 1, dest: "HELLO" }, { headers }),
                    transforms["keep-column"].func({ column: 0 }, { oHeaders, headers }),
                    transforms["add-column"].func({ column: "TEST"}, { oHeaders, headers, callCount: 0 }),
                    transforms["rename-column"].func({ src: 2, dest: "HELLO 2" }, { headers }),
                    transforms["add-column"].func({ column: "TEST"}, { oHeaders, headers, callCount: 1 }),
                    transforms["fill-column"].func({ column: 3, str: " YOU WAT 2" }, { headers }),
                    //transforms[0].func({ src: 0, dest: "HELLO" }, { headers }),
                    //transforms[1].func({ column: 0 }, { headers }),
                    //transforms[2].func({ column: "HELLO THERE" }, { headers }),
                    //transforms[3].func({ column: 1 }, { headers }),
                    //transforms[4].func({ src: 0, dest: 1 }, { headers })
                ]
            }

            let results = [];

            await reader.read(fileName, parts[0].start, parts[0].end, function(record) {
                console.log(record);
                processRow(info.transforms, results, false)(record);
            });

            console.log(oHeaders, results);
            console.log(outputToString(results, ","));

            done();
        })
    });
});
