const request = require("supertest");
const appCreator = require("../index.js");

let app;

beforeAll(async () => {
    app = await appCreator;
});

test('Test create transform', done => {
    request(app).post("/transforms").send({
        name: "reverse-column-2",
        label: "Reverse string in column",
        func: "(function reverseStr(str) { return str.split('').reverse().join(''); })"
    }).expect(200).then(() => done());
});

let mappingId = -1;
let mappingId2 = -1;

let validatorId_1 = -1;
let validatorId_2 = -1;

test('Test create validator', async () => {
    await request(app).post("/validators").send({
        name: "Test validator",
        description: "TEST DESC",
        columns: [
            "Test1",
            "Test2",
            "Test3"
        ]
    }).then(res => {
        validatorId_1 = res.body.id;
    });
    
    await request(app).post("/validators").send({
        name: "Test validator 2",
        description: "TEST DESC",
        columns: [
            "Test1",
            "Test2",
            "Test3"
        ]
    }).then(res => {
        validatorId_2 = res.body.id;
    });
});

test('Test create mapping', async done => {
    await request(app).post("/mappings").send({
        id: "recommendation_mapping_1",
        signature: "SIGNATURE",
        validator: validatorId_1
    }).expect(200).then(res => {
        mappingId = res.body.id;
    });

    request(app).post("/mappings").send({
        id: "recommendation_mapping_2",
        signature: "SIGNATURE",
        validator: validatorId_2
    }).expect(200).then(res => {
        mappingId2 = res.body.id;
        done();
    });
});

let instanceIds = [];

test('Test create instance', done => {
    let instances = [{
        name: "rename-column",
        params: {
            src: {
                columnHeader: "TEST_HEADER_1",
                value: 0
            },
            dest: {
                value: "NEW NAME"
            }
        }
    },
    {
        name: "rename-column",
        params: {
            src: {
                columnHeader: "5 TEST_HEADER_1",
                value: 0
            },
            dest: {
                value: "NEW NAME"
            }
        }
    },
    {
        name: "rename-column",
        params: {
            src: {
                columnHeader: "TEST_HEADER_2",
                value: 0
            },
            dest: {
                value: "NEW NAME 2"
            }
        }
    },
    {
        name: "keep-column",
        params: {
            column: {
                columnHeader: "TEST_HEADER_2",
                value: 0
            },
        }
    }];

    Promise.all(instances.map(x => request(app).post("/transforms/instances").send({
        mapping: mappingId,
        ...x
    }).expect(200).then(x => {
        return x.body.id;
    }))).then(instances => {
        instanceIds = instances;
        done();
    });
});

test('Test recommendations', done => {
    request(app).get("/recommendations?header=TEST_HEADER").expect(200).then(async res => {
        let recommendations = res.body;

        console.log(recommendations);

        //expect(recommendations).toHaveLength(3);
        done();
    });
});

test("Test delete transform", async () => {
    await request(app).delete("/transforms/reverse-column-2").expect(200);

    for (let id of instanceIds) {
        await request(app).delete("/transforms/instances/" + id).expect(200);
    }
});

test("Test delete mapping", async () => {
    await request(app).delete("/mappings/" + mappingId).expect(200);
    await request(app).delete("/mappings/" + mappingId2).expect(200);
});

afterAll(async () => {
    await app.close();
})
