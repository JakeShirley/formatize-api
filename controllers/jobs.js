const { adminOnly } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models, { jobs }, { s3, sqs }) {
    const { Job, TransformMapping } = models;

    app.post("/jobs-purge-queue", adminOnly, async (req, res) => {
        res.status(200).json(await sqs.purgeQueue({
            QueueUrl: process.env.QUEUE_URL
        }));
    });

    app.post('/job-groups', adminOnly, async (req, res) => {
        res.status(200).json(await jobs.createGroup(req.body));
    });

    app.get('/job-groups/:id', adminOnly, async (req, res) => {
        res.status(200).json(await jobs.getGroup({ id: req.params.id }));
    });

    app.get('/jobs/:id', adminOnly, function (req, res) {
        Job.findOne({ 
            where: { 
                id: req.params.id 
            },
            include: req.query.includeStages ? [{
                model: models.JobStage,
                as: "stages"
            }] : undefined
        }).then(result => {
            res.status(200).json(result);
        });
    });

    app.delete('/jobs/:id', adminOnly, function (req, res) {
        Job.destroy({ where: { id: req.params.id } }).then(result => {
            res.status(200).json(result);
        });
    });

    app.get('/jobs', adminOnly, function (req, res) {
        const { "file-name": filename } = req.query;

        let q = {
            order: [["id", "ASC"]],
        };

        if (filename) {
            q.where = {
                filename
            };
        }

        Job.findAll(q).then(result => {
            res.status(200).json(result);
        });
    });

    app.post('/jobs', adminOnly, async (req, res) => {
        let job = await jobs.create(req.body);

        res.status(200).json(job);
    });
}
