const { adminOnly } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models) {
    app.post("/pipelines", adminOnly, (req, res) => {
        const { Pipeline } = models;

        const { name } = req.body;

        Pipeline.create({
            name
        }).then(pipeline => {
            res.status(200).json(pipeline);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.get("/pipelines", adminOnly, (req, res) => {
        const { Pipeline } = models;

        Pipeline.findAll({
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
        }).then(pipelines => {
            res.status(200).json(pipelines);
        });
    });

    app.get("/pipelines/:id", adminOnly, (req, res) => {
        const { Pipeline } = models;

        const { id } = req.params;

        Pipeline.findOne({
            where: { id },
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
            include: [{ all: true, nested: true }],
        }).then(pipeline => {
            res.status(200).json(pipeline);
        });
    });

    app.patch("/pipelines/:id", adminOnly, (req, res) => {
        const { Pipeline } = models;

        const { id } = req.params;
        const { status } = req.body;

        Pipeline.update({
            status
        }, {
            where: { id }
        }).then(pipeline => {
            res.status(200).json(pipeline);
        });
    });

    app.delete("/pipelines/:id", adminOnly, (req, res) => {
        const { Pipeline } = models;

        const { id } = req.params;

        Pipeline.destroy({
            where: {
                id
            }
        }).then(() => {
            res.status(200).json();
        });
    });

    app.post("/pipelines/:id/data-source", adminOnly, (req, res) => {
        const { DataSource } = models;

        const { id, name, type } = req.body;

        DataSource.create({
            pipeline_id: req.params.id,
            id,
            name,
            type
        }).then(json => {
            res.status(200).json(json);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.patch("/pipelines/:id/data-source/:source", adminOnly, (req, res) => {
        const { DataSource } = models;

        const { id, name, type } = req.body;

        DataSource.update({
            id,
            name,
            type
        }, { where: {
            pipeline_id: req.params.id,
            id: req.params.source
        }}).then(json => {
            res.status(200).json(json);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.post("/pipelines/:id/mapping", adminOnly, (req, res) => {
        const { PipelineMapping } = models;

        const { mapping, dataSource } = req.body;

        PipelineMapping.create({
            pipeline_id: req.params.id,
            mapping_id: mapping,
            data_source_id: dataSource
        }).then(pipelineMapping => {
            pipelineMapping.reload({
                include: [{all: true, nested: true}]
            }).then(mapping => {
                res.status(200).json(mapping);
            })
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.delete("/pipelines/:id/mapping/:mappingId", adminOnly, (req, res) => {
        const { PipelineMapping } = models;

        PipelineMapping.destroy({
            where: {
                pipeline_id: req.params.id,
                mapping_id: req.params.mappingId
            }
        }).then(json => {
            res.status(200).json(json);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });
}
