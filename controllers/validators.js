const { adminOnly } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models, { validators }) {
    app.post("/validator-match", adminOnly, (req, res) => {
        validators.find(req.body).then(validator => {
            res.status(200).json(validator);
        });
    });

    app.post("/validators", adminOnly, (req, res) => {
        const { DataValidator } = models;

        const { name, description, columns } = req.body;

        DataValidator.create({
            name,
            description,
            columns
        }).then(validator => {
            res.status(200).json(validator);
        });
    });

    app.get("/validators", adminOnly, (req, res) => {
        const { DataValidator } = models;

        DataValidator.findAll({
            attributes:{
                exclude:['createdAt', 'updatedAt']
            }
        }).then(validators => {
            res.status(200).json(validators);
        });
    });

    app.get("/validators/:id", adminOnly, (req, res) => {
        const { DataValidator } = models;

        const { id } = req.params;

        DataValidator.findOne({
            where: { id },
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
            include: [{ all: true }],
        }).then(validator => {
            res.status(200).json(validator);
        });
    });

    app.patch("/validators/:id", adminOnly, (req, res) => {
        const { DataValidator } = models;

        const { id } = req.params;
        const { columns } = req.body;

        DataValidator.update({
            columns
        }, {
            where: { id },
            returning: true
        }).then(([validator]) => {
            res.status(200).json(validator);
        });
    });

    app.delete("/validators/:id", adminOnly, (req, res) => {
        const { DataValidator } = models;

        const { id } = req.params;

        DataValidator.destroy({
            where: {
                id
            }
        }).then(() => {
            res.status(200).json();
        });
    });
}
