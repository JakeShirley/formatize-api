const { adminOnly } = require("../middleware/jwt-helpers.js");
const Sequelize = require('sequelize');
const { Op } = Sequelize;

const LEVENSHTEIN_DISTANCE = 5;

module.exports = function (app, models, { schedules, clients }) {
    app.get("/recommendations", adminOnly, async (req, res) => {
        const { header } = req.query;
        const { TransformParamMetadata, TransformInstance, Transform } = models;

        //now using a fuzzy string matching algorithm (Levenshtein)

        let meta = await TransformParamMetadata.findAll({
            where: Sequelize.literal(`levenshtein(column_header, '` + decodeURIComponent(header) + `') < ${LEVENSHTEIN_DISTANCE}`),
            attributes: ["transform_instance_id"]
        });

        let instances = await TransformInstance.findAll({
            where: {
                id: {
                    [Op.in]: meta.map(x => x.transform_instance_id)
                }
            },
            attributes: ["params_json", [Sequelize.fn("COUNT", Sequelize.col("transform.name")), "score"]],
            order: [[Sequelize.col('score'), "DESC"]],
            group: ["transform.name", "TransformInstance.name", "TransformInstance.params_json"],
            include: [{
                model: Transform,
                as: "transform",
                attributes: ["name", "label", "params_json"],
            }]
        })

        res.status(200).json(instances).end();
    });
}