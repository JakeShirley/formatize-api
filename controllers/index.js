const { readdirSync } = require("fs");
const path = require('path');

module.exports = function (app, models, services, aws) {
    const directoryPath = path.join(__dirname);

    const files = readdirSync(directoryPath, { withFileTypes: true })
        .filter(dirent => !dirent.isDirectory())
        .map(dirent => dirent.name);

    files.forEach(function (file) {
        if (file != "index.js") {
            require(path.join(__dirname, file))(app, models, services, aws);
        }
    });
}
