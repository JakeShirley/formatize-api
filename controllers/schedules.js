const { adminOnly } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models, { schedules }) {
    app.post('/schedules/update', adminOnly, async (req, res) => {
        const { client, schedule } = req.body;

        const scheduleObj = await schedules.get({ id: schedule });

        if (!scheduleObj) {
            return res.status(400).json({
                message: "This schedule does not exist."
            });
        }

        if (!scheduleObj.recurring || (scheduleObj.end && new Date(scheduleObj.end) < new Date())) {
            return res.status(400).json({
                message: "This schedule has ended."
            });
        }

        const lastTimePeriod = await schedules.getLastTimePeriod({ scheduleId: schedule });

        res.status(200).json(await schedules.createTimePeriods({
            clientId: client,
            scheduleId: schedule,
            start: lastTimePeriod ? new Date(lastTimePeriod.end) : null,
            end: new Date()
        }));
    });

    app.post('/schedules', adminOnly, (req, res) => {
        const { Schedule } = models;

        const { name, description, start, end, duration, repeatEvery, repeatPeriod, recurring, backdated } = req.body;

        Schedule.create({
            name,
            description,
            start,
            end: end || (!recurring ? new Date((new Date(start)).getTime() + (duration * 24 * 3600 * 1000)) : null),
            duration,
            recurring,
            repeat_every: repeatEvery,
            repeat_period: repeatPeriod,
            backdated
        }).then(definition => {
            res.status(200).json(definition);
        });
    });

    app.get('/schedules', adminOnly, function (req, res) {
        const { Schedule } = models;

        Schedule.findAll().then(result => {
            res.status(200).json(result);
        });
    });

    app.get('/schedules/:id', adminOnly, function (req, res) {
        const { Schedule } = models;

        Schedule.findOne({ where: { id: req.params.id } }).then(result => {
            res.status(200).json(result);
        });
    });

    app.delete('/schedules/:id', adminOnly, function (req, res) {
        const { Schedule } = models;

        Schedule.destroy({ where: { id: req.params.id } }).then(result => {
            res.status(200).json(result);
        });
    });

    app.delete('/time-periods/:id', adminOnly, function (req, res) {
        const { TimePeriod } = models;

        TimePeriod.destroy({ where: { id: req.params.id } }).then(result => {
            res.status(200).json(result);
        });
    });
}
