const { adminOnly } = require("../middleware/jwt-helpers.js");

var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;

function getParamNames(funcStr) {
    var fnStr = funcStr.replace(STRIP_COMMENTS, '');
    var result = fnStr.slice(fnStr.indexOf('{')+1, fnStr.indexOf('}')).match(ARGUMENT_NAMES);
    if(result === null)
        result = [];
    return result;
}

module.exports = function (app, models) {
    app.get("/transforms", adminOnly, (req, res) => {
        const { Transform } = models;

        Transform.findAll({
            attributes: ["name", "label", "params_json", "locked"]
        }).then(transforms => {
            res.status(200).json(transforms);
        });
    });

    app.post("/transforms", adminOnly, (req, res) => {
        const { Transform } = models;

        const { name, label, func, params } = req.body;

        let newJSON = {};

        for (let param of getParamNames(func)) {
            newJSON[param] = {
                type: "text",
                label: param,
            }
        }

        Transform.create({
            name,
            label,
            func,
            params_json: JSON.stringify(params ? params : newJSON)
        }).then(transform => {
            res.status(200).json(transform);
        });
    });

    app.get("/transforms/instances/:mappingStage", adminOnly, (req, res) => {
        const { TransformInstance } = models;

        const { mappingStage } = req.params;

        TransformInstance.findAll({
            where: {
                mapping_stage_id: mappingStage
            },
            attributes: ["id", "name", "order", "params_json"]
        }).then(instances => {
            res.status(200).json(instances);
        });
    });

    app.post("/transforms/instances", adminOnly, async (req, res) => {
        const { TransformInstance, TransformParamMetadata } = models;

        const { mappingStage, name, order, params } = req.body;

        let instance = await TransformInstance.create({
            mapping_stage_id: mappingStage,
            name,
            order,
            params_json: JSON.stringify(params)
        });

        let paramNames = Object.keys(params);
        let toBeCreated = [];

        for (let paramName of paramNames) {
            let param = params[paramName];

            toBeCreated.push({
                transform_name: name,
                transform_instance_id: instance.id,
                column_header: param.columnHeader,
                value: typeof param.value === "undefined" ? param : param.value
            })
        }

        await TransformParamMetadata.bulkCreate(toBeCreated);

        res.status(200).json(instance);
    });

    app.patch("/transforms/instances/:id", adminOnly, (req, res) => {
        const { TransformInstance } = models;

        const { mappingStage, name, order, params } = req.body;


        let update = {};

        if (typeof mappingStage !== 'undefined')
            update.mapping_stage_id = mappingStage;
        if (typeof name !== 'undefined')
            update.name = name;
        if (typeof order !== 'undefined')
            update.order = order;
        if (typeof params !== 'undefined')
            update.params_json = JSON.stringify(params);

        TransformInstance.update(update, 
        {
            where: {
                id: req.params.id
            },
            returning: true
        }).then(([affectedRows, instance]) => {

            res.status(200).json(affectedRows);
        });
    });

    app.delete("/transforms/instances/:id", adminOnly, (req, res) => {
        const { TransformInstance } = models;

        const { id } = req.params;

        TransformInstance.destroy({
            where: {
                id
            }
        }).then(output => {
            res.status(200).end();
        });
    });

    app.get("/transforms/:name", adminOnly, (req, res) => {
        const { Transform } = models;

        const { name } = req.params;

        Transform.findOne({
            where: {
                name
            }
        }).then(transform => {
            res.status(200).json(transform);
        });
    });

    app.patch("/transforms/:name", adminOnly, (req, res) => {
        const { Transform } = models;

        const { name } = req.params;

        const { label, func, params_json } = req.body;

        let json = JSON.parse(params_json);
        let newJSON = {};

        for (let param of getParamNames(func)) {
            if (!json[param]) {
                newJSON[param] = {
                    type: "text",
                    label: param,
                }
            } else {
                newJSON[param] = json[param];
            }
        }

        Transform.update({
            label, func, params_json: JSON.stringify(newJSON)
        }, {
            where: {
                name,
                locked: false
            },
            returning: true
        }).then(([count, [transform]]) => {
            res.status(200).json(transform);
        });
    });

    app.delete("/transforms/:name", adminOnly, (req, res) => {
        const { Transform } = models;

        const { name } = req.params;

        Transform.destroy({
            where: {
                name,
                locked: false
            }
        }).then(() => {
            res.status(200).end();
        });
    });
}
