const BUCKET = process.env.BUCKET_NAME;
const csv = require("csv-parser");

module.exports = function (app, models, services, { s3 }) {
    app.get("/files/preview/:path", (req, res) => {
        let lines = req.query.lines || 20;
        if (lines < 1 || lines > 20) {
            res.status(400).json("Invalid number of lines. Must be between 1 and 20.");
        }

        S3ReadLinesInFile(s3, BUCKET, decodeURIComponent(req.params.path), 20).then(lines => {
            res.status(200).json(lines);
        });
    });

    app.get("/files", async (req, res) => {
        const { clientId } = req.params;
        const url = await s3.getSignedUrl('listObjects', {
            Bucket: BUCKET,
            Prefix: req.query.prefix ? req.query.prefix + "/" : undefined,
            Expires: 3600
        });

        res.status(200).send(url).end();
    });

    app.get("/files/:path", async (req, res) => {
        const { about } = req.query;
        const { clientId } = req.params;

        let url;

        if (about) {
            url = await s3.getSignedUrl('headObject', {
                Bucket: BUCKET,
                Key: decodeURIComponent(req.params.path),
                Expires: 3600
            });
        } else {
            url = await s3.getSignedUrl('getObject', {
                Bucket: BUCKET,
                Key: decodeURIComponent(req.params.path),
                Expires: 3600
            });
        }

        res.status(200).send(url);
    });

    app.post("/files/:path", async (req, res) => {
        const url = await s3.getSignedUrl('putObject', {
            Bucket: BUCKET,
            Key: decodeURIComponent(req.params.path),
            ContentType: req.headers["content-type"],
            Expires: 3600
        });

        res.status(200).send(url);
    });

    app.delete("/files/:path", async (req, res) => {
        const url = await s3.getSignedUrl('deleteObject', {
            Bucket: BUCKET,
            Key: decodeURIComponent(req.params.path),
            Expires: 3600
        });

        res.status(200).send(url);
    });
}
