const { adminOnly } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models) {
    app.post("/mappings", adminOnly, (req, res) => {
        const { TransformMapping, TransformMappingStage } = models;

        const { id, signature, validator } = req.body;

        TransformMapping.create({
            id,
            signature,
            data_validator_id: validator
        }).then(mapping => {
            TransformMappingStage.create({
                mapping_id: mapping.id,
                name: "Start stage",
                description: "The initial stage of the mapping"
            }).then(stage => {
                res.status(200).json(mapping);
            })
        });
    });

    app.get("/mappings", adminOnly, (req, res) => {
        const { TransformMapping } = models;

        TransformMapping.findAll({
            attributes:{
                exclude:['createdAt', 'updatedAt']
            }
        }).then(mappings => {
            res.status(200).json(mappings);
        });
    });

    app.get("/mappings/:id", adminOnly, (req, res) => {
        const { TransformMapping } = models;

        const { id } = req.params;

        TransformMapping.findOne({
            where: { id },
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
            include: [{ all: true }],
        }).then(mapping => {
            res.status(200).json(mapping);
        });
    });

    app.patch("/mappings/:id", adminOnly, (req, res) => {
        const { TransformMapping } = models;

        const { id } = req.params;
        const { validator } = req.body;

        TransformMapping.update({
            data_validator_id: validator
        }, {
            where: { id }
        }).then(mapping => {
            res.status(200).json(mapping);
        });
    });

    app.delete("/mappings/:id", adminOnly, (req, res) => {
        const { TransformMapping } = models;

        const { id } = req.params;

        TransformMapping.destroy({
            where: {
                id
            }
        }).then(() => {
            res.status(200).json();
        });
    });

    app.post("/mapping-stages", adminOnly, (req, res) => {
        const { TransformMappingStage } = models;

        const { mapping, name, description } = req.body;

        TransformMappingStage.create({
            mapping_id: mapping,
            name,
            description
        }).then(stage => {
            res.status(200).json(stage);
        });
    });

    app.delete("/mapping-stages/:id", adminOnly, (req, res) => {
        const { TransformMappingStage } = models;

        const { id } = req.params;

        TransformMappingStage.destroy({
            where: {
                id: id
            }
        }).then(stage => {
            res.status(200).json(stage);
        });
    });
}
