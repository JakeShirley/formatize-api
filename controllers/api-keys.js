const uuidv5 = require('uuid/v5');

module.exports = function (app, models) {
    const { ClientAPIKey } = models;

    app.post('/api-keys', function (req, res) {
        ClientAPIKey.count({}).then(num => {
            console.log(req.user);
            ClientAPIKey.create({
                api_key: uuidv5(req.user.sub + num, "9779332c-67ad-40a2-91d4-910364405fb3"),
                user_id: req.user.uid
            }).then(apiKey => {
                res.status(200).json({id: apiKey.id, api_key: apiKey.api_key});
            }).catch(err => {
                console.log(err);
                res.status(500).json(err);
            });
        });
    });

    app.get('/api-keys', function (req, res) {
        ClientAPIKey.findAll({
            where: {
                user_id: req.user.uid
            },
            attributes: ["id", "api_key"]
        }).then(keys => {
            res.status(200).json(keys);
        });
    });

    app.delete('/api-keys/:id', function (req, res) {
        ClientAPIKey.destroy({
            where: {
                id: req.params.id
            }
        }).then(apiKey => {
            res.status(200).send();
        });
    });
}
