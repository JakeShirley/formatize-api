module.exports = async function (app, models, { jobs, clients, worker }) {
    if (process.env.NODE_ENV === "worker") {
        app.post("/", async (req, res) => {
            if (req.body) {
                if (await worker.process(req.body)) {
                    return res.status(200).send();
                }
            }

            return res.status(201).send();
        });
    }
}
