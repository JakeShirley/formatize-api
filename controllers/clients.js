const { adminOnly, claimMatchesParam } = require("../middleware/jwt-helpers.js");

module.exports = function (app, models, { schedules, clients }) {
    app.post("/clients", adminOnly, (req, res) => {
        const { Client } = models;

        const { name, status } = req.body;

        Client.create({
            name,
            status
        }).then(client => {
            res.status(200).json(client);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.get("/clients", adminOnly, (req, res) => {
        const { Client } = models;

        Client.findAll({
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
        }).then(clients => {
            res.status(200).json(clients);
        });
    });

    app.get("/clients/:id", claimMatchesParam("organization", "id"), (req, res) => {
        const { Client } = models;

        const { id } = req.params;

        Client.findOne({
            where: { id },
            attributes:{
                exclude:['createdAt', 'updatedAt']
            },
            include: [
            {
                model: models.ClientPipelineMapping,
                as: "pipeline_list",
                include: [{
                    model: models.Pipeline,
                    as: "pipeline"
                },
                {
                    model: models.Schedule,
                    as: "schedule"
                }]
            },
            {
                model: models.TimePeriod,
                as: "time_periods",
                include: [{
                    model: models.Schedule,
                    as: "schedule"
                }]
            }],
        }).then(client => {
            res.status(200).json(client);
        });
    });

    app.patch("/clients/:id", adminOnly, (req, res) => {
        const { Client } = models;

        const { id } = req.params;
        const { name, status } = req.body;

        Client.update({
            name,
            status
        }, {
            where: { id }
        }).then(client => {
            res.status(200).json(client);
        });
    });

    app.delete("/clients/:id", adminOnly, (req, res) => {
        const { Client } = models;

        const { id } = req.params;

        Client.destroy({
            where: {
                id
            }
        }).then(() => {
            res.status(200).json();
        });
    });

    app.get("/clients/:id/time-periods", claimMatchesParam("organization", "id"), (req, res) => {
        const { TimePeriod } = models;

        TimePeriod.findAll({
            where: {
                client_id: req.params.id
            }
        }).then(timePeriods => {
            res.status(200).json(timePeriods);
        });
    });

    app.get("/clients/:id/time-periods/:timePeriod", claimMatchesParam("organization", "id"), (req, res) => {
        const { TimePeriod } = models;

        TimePeriod.findOne({
            where: {
                id: req.params.timePeriod
            },
            include: [{all: true, nested: true}]
        }).then(timePeriod => {
            res.status(200).json(timePeriod);
        });
    });

    app.get("/clients/:id/time-periods/:timePeriod/data-sources", claimMatchesParam("organization", "id"), async (req, res) => {
        const { id, timePeriod } = req.params;

        res.status(200).json(await clients.getDataSourcesByTimePeriod({ client: id, timePeriod }));
    });

    app.post("/clients/:id/pipelines", claimMatchesParam("organization", "id"), (req, res) => {
        const { ClientPipelineMapping, TimePeriod } = models;

        const { pipeline, schedule } = req.body;

        ClientPipelineMapping.create({
            client_id: req.params.id,
            pipeline_id: pipeline,
            schedule_id: schedule
        }).then(pipelineMapping => {
            pipelineMapping.reload({
                include: [{all: true, nested: true}]
            }).then(async pipelineMapping => {
                res.status(200).json(await schedules.createTimePeriods({
                    clientId: req.params.id,
                    scheduleId: schedule,
                    mockDate: new Date(req.query.mockDate)
                }));
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });

    app.delete("/clients/:id/pipelines/:pipelineId", claimMatchesParam("organization", "id"), (req, res) => {
        const { ClientPipelineMapping } = models;

        ClientPipelineMapping.destroy({
            where: {
                client_id: req.params.id,
                pipeline_id: req.params.pipelineId,
            }
        }).then(pipelineMapping => {
            res.status(200).json(pipelineMapping);
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
    });
}
