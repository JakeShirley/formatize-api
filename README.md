# Formatize API
## Environment
This application was tested in **Node v12.13.0**. Older versions may work but are not supported.

The environment variables that need to be set are:

* BUCKET_NAME=[AWS S3 bucket name]
* QUEUE_URL=[AWS queue]
* OKTA_ISSUER=[Okta issuer]
* OKTA_AUD=[Okta audience]
* OKTA_CLIENT_ID=[ID of the application in Okta]
* NODE_ENV=[development/test/production]
* MAXIMUM_JOBS=[maximum number of concurrent jobs]
## How to run

To run you first have to download all the dependencies. Run **npm install**, and then **npm start** to run the code. 

## Testing
The tests require Jest to be installed globally to run. Run **npm test** which will execute Jest with the environment variables below:
* NODE_ENV=test
* BUCKET_NAME=data-aggregator-bucket-dev
* QUEUE_URL=https://sqs.eu-west-1.amazonaws.com/886268689675/formatize-dev

Change these to use your own AWS configuration.
### CI342 - Recommendation system test
Run **npm test -- recommendations** to test the recommendations API. This test will not require the BUCKET_NAME or QUEUE_URL environment variables to be set.