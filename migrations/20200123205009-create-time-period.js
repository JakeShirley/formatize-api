'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TimePeriod', {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true
        },
        schedule_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Schedule',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        client_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Client',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        start: Sequelize.DATEONLY,
        end: Sequelize.DATEONLY,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('TimePeriod');
  }
};
