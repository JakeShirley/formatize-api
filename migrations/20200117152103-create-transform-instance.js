'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('TransformInstance', {
          id: {
              type: Sequelize.INTEGER,
              autoIncrement: true,
              primaryKey: true
          },
          mapping_stage_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'TransformMappingStage',
                  key: 'id'
              },
              onDelete: 'CASCADE'
          },
          name: {
              type: Sequelize.STRING,
              references: {
                  model: 'Transform',
                  key: 'name'
              },
              onDelete: 'CASCADE'
          },
          order: Sequelize.INTEGER,
          params_json: Sequelize.STRING,
          createdAt: Sequelize.DATE,
          updatedAt: Sequelize.DATE,
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TransformInstance');
  }
};
