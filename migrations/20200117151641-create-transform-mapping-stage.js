'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('TransformMappingStage', {
          id: {
              type: Sequelize.INTEGER,
              autoIncrement: true,
              primaryKey: true
          },
          mapping_id: {
              type: Sequelize.STRING,
              references: {
                  model: 'TransformMapping',
                  key: 'id'
              },
              onDelete: 'CASCADE'
          },
          name: Sequelize.STRING,
          description: Sequelize.STRING,
          clone_data: Sequelize.BOOLEAN,
          createdAt: Sequelize.DATE,
          updatedAt: Sequelize.DATE,
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TransformMappingStage');
  }
};
