'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PipelineMapping', {
        pipeline_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            references: {
                model: 'Pipeline',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        mapping_id: {
            type: Sequelize.STRING,
            primaryKey: true,
            references: {
                model: 'TransformMapping',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        data_source_id: {
            type: Sequelize.STRING,
            primaryKey: true,
            references: {
                model: 'DataSource',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PipelineMapping');
  }
};
