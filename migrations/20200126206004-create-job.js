'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Job', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        aws_upload_id: Sequelize.STRING,
        aws_etag: Sequelize.STRING,
        status: Sequelize.STRING,
        started_by: Sequelize.STRING,
        started_at: Sequelize.DATE,
        finished_at: Sequelize.DATE,
        filename: Sequelize.STRING,
        output_filename: Sequelize.STRING,
        preview: Sequelize.BOOLEAN,
        valid: Sequelize.BOOLEAN,
        mapping_id: {
            type: Sequelize.STRING,
            references: {
                model: 'TransformMapping',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        mapping_stage_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'TransformMappingStage',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        job_group_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'JobGroup',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Job');
  }
};
