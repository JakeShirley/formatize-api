'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DataSource', {
        id: {
            type: Sequelize.STRING,
            primaryKey: true,
        },
        pipeline_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Pipeline',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        type: Sequelize.STRING,
        name: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DataSource');
  }
};
