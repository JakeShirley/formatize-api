'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('JobPart', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        job_stage_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'JobStage',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        status: Sequelize.STRING,
        part: Sequelize.INTEGER,
        aws_etag: Sequelize.STRING,
        start_byte: Sequelize.BIGINT,
        end_byte: Sequelize.BIGINT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('JobPart');
  }
};
