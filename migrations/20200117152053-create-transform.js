'use strict';

const coreTransforms = require("../common/transform-functions.js");

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('Transform', {
          name: {
              type: Sequelize.STRING,
              primaryKey: true
          },
          owner: Sequelize.STRING,
          label: Sequelize.STRING,
          order: Sequelize.INTEGER,
          params_json: {
            type: Sequelize.TEXT,
            defaultValue: ""
          },
          func: Sequelize.TEXT,
          locked: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
          },
          createdAt: Sequelize.DATE,
          updatedAt: Sequelize.DATE,
      }).then(() => {
          return queryInterface.bulkInsert('Transform', Object.keys(coreTransforms).map(x => {
            let t = coreTransforms[x];

            return {
                owner: 'ADMIN',
                name: x,
                label: t.label,
                order: t.order,
                params_json: JSON.stringify(t.params),
                func: t.func.toString(),
                locked: true,
                createdAt: new Date(),
                updatedAt: new Date()
            };
          }));
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Transform');
  }
};
