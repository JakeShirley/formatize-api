'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DataValidatorColumn', {
        validator_id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          references: {
              model: 'DataValidator',
              key: 'id'
          },
          onDelete: 'CASCADE'
        },
        name: {
          type: Sequelize.STRING,
          primaryKey: true
        }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DataValidatorColumn');
  }
};
