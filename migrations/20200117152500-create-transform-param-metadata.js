'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('TransformParamMetadata', {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          transform_name: {
            type: Sequelize.STRING,
            references: {
              model: 'Transform',
              key: 'name'
            },
            onDelete: 'CASCADE'
          },
          transform_instance_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'TransformInstance',
              key: 'id'
            },
            onDelete: 'CASCADE'
          },
          column_header: Sequelize.STRING,
          type: Sequelize.STRING,
          value: Sequelize.TEXT,
          createdAt: Sequelize.DATE,
          updatedAt: Sequelize.DATE,
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TransformParamMetadata');
  }
};
