'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Schedule', {
        id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: Sequelize.STRING,
        start: {
            type: Sequelize.DATE,
            allowNull: false
        },
        end: {
            type: Sequelize.DATE,
        },
        duration: Sequelize.INTEGER,
        recurring: Sequelize.BOOLEAN,
        repeat_every: Sequelize.INTEGER,
        repeat_period: Sequelize.STRING,
        backdated: Sequelize.BOOLEAN,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('Schedule');
  }
};
