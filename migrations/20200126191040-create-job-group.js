'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('JobGroup', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        status: Sequelize.STRING,
        client_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Client',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        time_period_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'TimePeriod',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        job_count: Sequelize.INTEGER,
        output_name: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('JobGroup');
  }
};
