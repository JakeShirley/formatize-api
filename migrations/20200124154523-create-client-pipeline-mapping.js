'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ClientPipelineMapping', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            unique: true
        },
        client_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: 'Client',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        pipeline_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: 'Pipeline',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        schedule_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: {
                model: 'Schedule',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ClientPipelineMapping');
  }
};
