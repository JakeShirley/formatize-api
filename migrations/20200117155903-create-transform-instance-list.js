'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('TransformInstanceList', {
          id: {
              type: Sequelize.INTEGER,
              autoIncrement: true,
              primaryKey: true
          },
          transform_instance: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'TransformInstance',
                  key: 'id'
              },
              onDelete: 'CASCADE'
          }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TransformInstanceList');
  }
};
