'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('TransformMapping', {
          id: {
              type: Sequelize.STRING,
              primaryKey: true
          },
          data_validator_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'DataValidator',
                  key: 'id'
              },
              onDelete: 'SET NULL'
          },
          name: Sequelize.STRING,
          description: Sequelize.TEXT,
          signature: Sequelize.TEXT,
          createdAt: Sequelize.DATE,
          updatedAt: Sequelize.DATE,
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TransformMapping');
  }
};
