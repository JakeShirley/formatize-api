'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('JobStage', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        job_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Job',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        status: Sequelize.STRING,
        mapping_stage_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'TransformMappingStage',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        stage: Sequelize.INTEGER,
        filename: Sequelize.STRING,
        output_filename: Sequelize.STRING,
        aws_upload_id: Sequelize.STRING,
        aws_etag: Sequelize.STRING,
        started_at: Sequelize.DATE,
        finished_at: Sequelize.DATE,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('JobStage');
  }
};
