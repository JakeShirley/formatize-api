'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('User', {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            okta_id: {
                type: Sequelize.STRING,
            },
            client_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Client',
                    key: 'id'
                },
                onDelete: 'CASCADE'
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('User');
    }
};
