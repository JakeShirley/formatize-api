module.exports = function (models, services, { s3, sqs }) {
    class Validators {
        constructor() {
            this.key = "validators";
        }

        find({ columns }) {
            const { DataValidator } = models;
                
            return DataValidator.findOne({
                where: {
                    columns
                }
            });
        }

        findMappings({ columns }) {   
            const { DataValidator, TransformMapping } = models;
             
            return TransformMapping.findAll({
                include: [{
                    model: DataValidator,
                    as: "validator",
                    where: {
                        columns 
                    }
                }]
            });
        }
    }

    return new Validators();
}
