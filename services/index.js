const { readdirSync } = require("fs");
const path = require('path');

module.exports = function (models, aws) {
    const services = {};

    const directoryPath = path.join(__dirname);

    const files = readdirSync(directoryPath, { withFileTypes: true })
        .filter(dirent => !dirent.isDirectory())
        .map(dirent => dirent.name);

    files.forEach(function (file) {
        if (file != "index.js") {
            let service = require(path.join(__dirname, file))(models, services, aws);

            services[service.key] = service;
        }
    });

    return services;
}
