const QUEUE_URL = process.env.QUEUE_URL;

const { processRow, outputToString, CSVReader, XLSXReader } = require("../common/processor.js");

const redis = require("redis");
const retry = require("promise-retry");

const Sequelize = require("sequelize");
const { Op } = Sequelize;

var client;
if (process.env.NODE_ENV != "production") {
    client = redis.createClient({
        host: process.env.REDIS_HOST || "127.0.0.1",
        port: process.env.REDIS_PORT || 6379
    });
}

let jobsCount = 0;

module.exports = function (models, { jobs, clients, validators }, { s3, sqs }) {
    let readers = {
        "csv": new CSVReader(s3),
        "txt": new CSVReader(s3),
        "xls*": new XLSXReader(s3)
    }

    function getReader(filename) {
        let readerIds = Object.keys(readers);
        let type = filename.split(".").pop();
        let foundType = readerIds.find(x => RegExp(x).test(type));

        return readers[foundType];
    }

    async function runJob(s3, sqs, models, { jobs, validators }, jobId) {
        let job = await jobs.get({ id: jobId, status: "NOT_STARTED" });

        if (job) {
            job.status = "STARTED";
            job.started_at = new Date();
            job.save();

            let mapping = job.mapping;
            let headers;

            if (!mapping || mapping.data_validator_id !== null) {
                headers = await getReader(job.filename).getHeaders(job.filename);
            }

            if (!mapping) {
                let mappings = await validators.findMappings({ columns: headers });

                if (mappings.length === 0) {
                    job.status = "NO_MAPPING_FOUND";
                    job.save();
                    return;
                }

                mapping = mappings[0];
                job.mapping_id = mapping.id;
                await job.save();
            } else if (mapping.data_validator_id !== null) {
                await job.save();

                let validator = await models.DataValidator.findOne({
                    where: {
                        id: mapping.data_validator_id
                    }
                });

                for (let col of validator.columns) {
                    if (!headers.includes(col)) {
                        job.status = "VALIDATION_ERROR";
                        await job.save();
                        return;
                    }
                }
            } else {
                job.save();
            }

            const stages = await models.TransformMappingStage.findAll({
                where: {
                    mapping_id: mapping.id
                }
            });

            console.log(mapping.id);

            let stageNum = 1;
            let firstStage = null;

            for (let stage of stages) {
                let jobStage = await models.JobStage.create({
                    job_id: job.id,
                    mapping_stage_id: stage.id,
                    stage: stageNum,
                    status: "NOT_STARTED"
                });

                firstStage = firstStage || jobStage;

                stageNum++;
            }

            const params = {
                DelaySeconds: 1,
                MessageBody: JSON.stringify({
                    jobStageId: firstStage.id,
                }),
                QueueUrl: process.env.QUEUE_URL
            };

            try {
                await sqs.sendMessage(params).promise();
            } catch (e) {
                console.log("Error sending sqs message", e);
            }
        }
    }

    function jobStageFileName(filename, mapping, stage) {
        const [rootFolder, clientId, generatedOrUploaded, timePeriod, dataSource] = filename.split("/");

        const name = filename.split("/").pop();

        if (rootFolder === "clients") {
            return "clients/" + clientId + "/generated/" + timePeriod + "/" + dataSource + "/" + mapping + "/" + stage + "/" + name + ".csv";
        } else {
            return rootFolder + "/" + clientId + "/generated/" + mapping + "/" + stage + "/" + name + ".csv";
        }
    }

    async function runJobStage(s3, sqs, models, { clients, jobs }, jobStageId) {
        const { JobStage, JobPart } = models;

        console.log("STARTING STAGE");

        let jobStage = await JobStage.findOne({
            where: {
                id: jobStageId,
                status: "NOT_STARTED"
            },
            include: [{all: true, nested: true}]
        });

        jobStage.status = "STARTED";
        jobStage.started_at = new Date();
        jobStage.save();

        const { job } = jobStage;

        job.status = "STARTED";
        job.save();

        const { filename } = job;

        console.log(job.mapping_id);

        const outputName = jobStageFileName(filename, job.mapping_id, jobStage.stage);

        try {
            await s3.deleteObject({
                Key: outputName
            }).promise();
        } catch (e) {}

        const { UploadId } = await s3.createMultipartUpload({
            Key: outputName
        }).promise();

        jobStage.aws_upload_id = UploadId;
        await jobStage.save();

        const loadableFile = jobStage.stage == 1 ? job.filename : jobStageFileName(filename, job.mapping_id, jobStage.stage - 1);

        for (var i = 0; i < 10; i++) {
            try {
                await s3.headObject({ 
                    Key: loadableFile
                }).promise();

                await new Promise((good, bad) => setTimeout(() => good(), 5000));
                break;
            } catch (e) {
                console.log("Error: ", e);
            }
        }

        const parts = await getReader(loadableFile).getPartByteRanges(loadableFile, process.env.PART_SIZE_MB || 32);

        for (let i = 0; i < parts.length; i++) {
            let part = parts[i];
            let jobPart = await JobPart.create({
                job_stage_id: jobStage.id,
                part: i + 1,
                start_byte: part.start,
                end_byte: part.end,
                status: "NOT_STARTED"
            });

            var params = {
                DelaySeconds: 1,
                MessageBody: JSON.stringify({
                    jobPartId: jobPart.id,
                }),
                QueueUrl: QUEUE_URL
            };

            await sqs.sendMessage(params).promise();

            //jobParts.push(jobPart);
        }
    }

    async function runJobPart(s3, sqs, models, { jobs, clients }, jobPartId) {
        const { Job, JobPart, JobStage, PipelineMapping, TransformMapping, TransformMappingStage } = models;

        let jobPart = await JobPart.findOne({
            where: {
                id: jobPartId,
                status: "NOT_STARTED"
            },
            include: [{all: true, nested: true}]
        });

        const jobStage = jobPart.job_stage;
        const job = jobStage.job;

        console.log("START JOB STAGE " + jobStage.id + "(" + jobPart.part + ")");

        jobPart.status = "STARTED";
        jobPart.save();

        const { filename, mapping_id } = job;
        const { mapping_stage_id, aws_upload_id } = jobStage;
        const { start_byte, end_byte } = jobPart;

        let mappingStage = await TransformMappingStage.findOne({
            where: {
                id: mapping_stage_id
            },
            order: [
                [{ model: models.TransformInstance, as: 'transforms' }, "order", "ASC"]
            ],
            include: {
                model: models.TransformInstance,
                as: "transforms",
                include: [{all: true, nested: true}]
            }
        });

        let transforms = [];


        const loadableFile = jobStage.stage == 1 ? filename : jobStageFileName(filename, job.mapping_id, jobStage.stage - 1);

        let oHeaders = await getReader(loadableFile).getHeaders(loadableFile);
        let headers = [];

        let calls = {};

        for (let t of mappingStage.transforms) {
            calls[t.transform.name] = (calls[t.transform.name] || 0) + 1;

            let func = eval("(" + t.transform.func + ")");
            let params = JSON.parse(t.params_json);

            params = Object.keys(params).map(x => {
                return { [x]: params[x].value};
            }).reduce((acc, val) => {
                acc = { ...acc, ...val };
                return acc;
            }, {});

            transforms.push(func(params, { oHeaders, headers, callCount: calls[t.transform.name] }));
        }

        let group = await jobs.getGroup({ id: job.job_group_id });

        let results = [headers];

        await getReader(loadableFile).read(loadableFile, start_byte, end_byte, function(record) {
            processRow(transforms, results)(record);
        }).catch(err => {
            jobPart.status = "ERROR";
            jobPart.save();

            jobStage.status = "ERROR";
            jobStage.finished_at = new Date();
            jobStage.save();

            job.status = "ERROR";
            job.finished_at = new Date();
            job.save();

            console.log("Error: Probably missing file.");
            console.log(err);
        });

    
        let csv = outputToString(results, ",");
        const outputName = jobStageFileName(filename, job.mapping_id, jobStage.stage);

        await new Promise((good, bad) => {
            client.sadd(outputName, csv, function (err, res) {
                if (err) {
                    return bad(err);
                }

                good(res);
            });
        });

        jobPart.status = "DONE";
        //jobPart.aws_etag = ETag;
        await jobPart.save();

        let allParts = await JobPart.findAll({where:
            {
                job_stage_id: jobStage.id,
            },
            attributes: ["aws_etag", "part", "status"]
        });

        if (allParts.length == allParts.filter(x => x.status === "DONE").length) {
            function pop() {
                return new Promise((good, bad) => {
                    client.spop(outputName, function(err, reply) {
                        if (err)
                            return bad(err);

                        good(reply);
                    });
                });
            }

            let Parts = [];
            let str = "";
            let PartNum = 1;
            for (let i = 0; i < allParts.length; i++) {
                str += await pop();

                if (i == allParts.length - 1 || Buffer.byteLength(str, 'utf8') > (5 * 1024 * 1024)) {
                    let { ETag } = await s3.uploadPart({
                        Key: outputName,
                        UploadId: aws_upload_id,
                        Body: str,
                        PartNumber: PartNum }).promise();

                    Parts.push({
                        ETag,
                        PartNumber: PartNum
                    });

                    if (i == allParts.length - 1) {
                        console.log("Outputing: ", outputName);
                        
                        const multipartParams = {
                            Key: outputName,
                            MultipartUpload: {
                                Parts: Parts.sort((a, b) => a.PartNumber - b.PartNumber)
                            },
                            UploadId: aws_upload_id
                        };

                        console.log("STARTING MULTI-PART UPLOAD");

                        await s3.completeMultipartUpload(multipartParams).promise();

                        await retry(retry => s3.headObject({
                            Key: outputName
                        }).promise().catch(retry), { minTimeout: 5000 });

                        console.log("Finish multi-part upload");

                        break;
                    }

                    str = "";
                    PartNum++;
                }
            }

            jobStage.status = "DONE";
            jobStage.finished_at = new Date();
            jobStage.output_filename = outputName;
            await jobStage.save();

            console.log("DONE STAGE");

            let stages = await JobStage.findAll({where:
                {
                    job_id: job.id
                },
                attributes: ["id", "output_filename", "status"],
                order: [
                    ['stage', 'DESC'],
                ],
            });

            if (stages.length === 1 || stages.length === stages.filter(x => x.status == "DONE").length) {
                job.status = "DONE";
                job.finished_at = new Date();
                job.output_filename = outputName;
                await job.save();

                if (job.job_group_id) {
                    await group.reload();
                    if (group.job_count == group.jobs.filter(x => x.status === "DONE").length) {
                        const params = {
                            DelaySeconds: 1,
                            MessageBody: JSON.stringify({
                                jobGroupId: group.id,
                            }),
                            QueueUrl: QUEUE_URL
                        };
                
                        await sqs.sendMessage(params).promise();
                    }
                }
            } else {
                const params = {
                    DelaySeconds: 1,
                    MessageBody: JSON.stringify({
                        jobStageId: stages.find(x => x.status == "NOT_STARTED").id,
                    }),
                    QueueUrl: QUEUE_URL
                };

                await sqs.sendMessage(params).promise();
            }
        }

        console.log("Finished part");

        jobsCount--;
    }

    async function combineJobGroup(s3, sqs, models, { jobs, clients }, jobGroupId) {
        const { Job, JobPart, JobStage, PipelineMapping, TransformMapping, TransformMappingStage } = models;

        let group = await jobs.getGroup({ id: jobGroupId });

        group.status = "STARTED_COMBINING";
        group.save();

        const outputName = group.output_name;

        const { UploadId } = await s3.createMultipartUpload({
            Key: outputName
        }).promise();

        let str = "";
        let partNum = 1;
        let Parts = [];

        for (let i = 0; i < group.jobs.length; i++) {
            let job = group.jobs[i];
            let data;
            try {
                data = await s3.getObject({
                    Key: job.output_filename
                }).promise().then(res => res.Body.toString());
            } catch (e) {
                jobsCount--;
                return;
            }

            if (i != 0) {
                let endOfFirstLine = data.indexOf("\n");

                str += data.substring(endOfFirstLine).trim() + "\n";
            } else {
                str += data.trim() + "\n";
            }

            if (i === group.jobs.length - 1 || Buffer.byteLength(str, 'utf8') > (5 * 1024 * 1024)) {
                let { ETag } = await s3.uploadPart({
                    Key: outputName,
                    UploadId,
                    Body: str,
                    PartNumber: partNum
                }).promise();

                Parts.push({
                    PartNumber: partNum++,
                    ETag
                });

                if (i === group.jobs.length - 1) {
                    const multipartParams = {
                        Key: outputName,
                        MultipartUpload: {
                            Parts: Parts.sort((a, b) => a.PartNumber - b.PartNumber)
                        },
                        UploadId
                    };

                    await s3.completeMultipartUpload(multipartParams).promise();
                }

                str = "";
            }
        }

        console.log("COMBINING");

        group.status = "DONE";
        await group.save();

        jobsCount--;
    }

    class Worker {
        constructor() {
            this.key = "worker";
            this.maxJobs = 1;
        }

        setMaximumJobs(maxJobs) {
            this.maxJobs = maxJobs;
        }

        async process(message) {
            if (message.jobId) {
                runJob(s3, sqs, models, { jobs, clients, validators }, message.jobId);
                return true;
            } else if (message.jobStageId) {
                runJobStage(s3, sqs, models, { jobs, clients }, message.jobStageId);
                return true;
            } else if (message.jobPartId && jobsCount < this.maxJobs) {
                console.log("Running part");
                jobsCount++;
                runJobPart(s3, sqs, models, { jobs, clients }, message.jobPartId);
                return true;
            } else if (message.jobGroupId && jobsCount < this.maxJobs) {
                console.log("Running combiner");
                jobsCount++;
                combineJobGroup(s3, sqs, models, { jobs, clients }, message.jobGroupId);
                return true;
            } else if (message.Records) {
                for (var rec of message.Records) {
                    const { key, eTag } = rec.s3.object;
                    const formattedKey = decodeURIComponent(key.replace(/\+/g, '%20'));
                    const [rootFolder, clientId, generatedOrUploaded, timePeriodId, dataSourceId] = formattedKey.split("/");

                    if (rootFolder == "clients" && generatedOrUploaded != "generated") {
                        if (rec.eventName == "ObjectCreated:Put") {
                            
                            const { ClientPipelineMapping, PipelineMapping, TransformMapping, TimePeriod } = models;

                            let { schedule_id } = await TimePeriod.findOne({
                                where: {
                                    id: timePeriodId
                                },
                                attributes: ["schedule_id"]
                            });

                            let pipelines = await ClientPipelineMapping.findAll({
                                where: {
                                    client_id: clientId,
                                    schedule_id: schedule_id
                                },
                                attributes: ["pipeline_id"]
                            });


                            const pipelineMappings = await PipelineMapping.findAll({
                                where: {
                                    pipeline_id: pipelines.map(x => x.pipeline_id),
                                    data_source_id: dataSourceId
                                },
                                include: [
                                    {
                                        model: TransformMapping,
                                        as: "mapping",
                                        attributes: ["id"]
                                    }
                                ],
                                attributes: []
                            });

                            for (let pipelineMapping of pipelineMappings) {
                                const { mapping } = pipelineMapping;

                                let dataSources = await clients.getDataSourcesByTimePeriod({
                                    client: clientId,
                                    timePeriod: timePeriodId
                                });

                                let group = await jobs.getOrCreateGroup({
                                    client: clientId,
                                    timePeriod: timePeriodId,
                                    jobCount: dataSources.length,
                                    outputName: `clients/${clientId}/generated/${timePeriodId}/combined/output.csv`
                                });

                                console.log("CREATING JOB", formattedKey);


                                jobs.create({
                                    group: group.id,
                                    mapping: mapping.id,
                                    eTag, 
                                    filename: formattedKey 
                                });
                            }
                        } else if (rec.eventName == "ObjectRemoved:DeleteMarkerCreated" || rec.eventName == "ObjectRemoved:Delete") {
                            await jobs.makeJobsWithFilenameInvalid({
                                filename: formattedKey
                            });
                        }
                    }
                    return true;
                }
            }

            return false;
        }

        processFromQueue(queue) {

        }

        stop() {
            return new Promise((good, bad) => {
                if (client) {
                    client.quit(err => err ? bad(err) : good());
                    client = null;
                }
            });
        }
    }

    return new Worker();
}
