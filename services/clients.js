module.exports = function (models, services, { s3, sqs }) {
    class Clients {
        constructor() {
            this.key = "clients";
        }

        getDataSourcesByTimePeriod({ client, timePeriod }) {
            const { TimePeriod, ClientPipelineMapping } = models;
    
            return TimePeriod.findOne({
                where: {
                    id: timePeriod
                },
                include: [{all: true}]
            }).then(timePeriod => {
                return ClientPipelineMapping.findAll({
                    where: {
                        client_id: client,
                        schedule_id: timePeriod.schedule.id
                    },
                    include: [{all: true, nested: true}]
                }).then(pipelineMappings => {
                    return pipelineMappings.reduce((acc, val) => {
                        acc = [...acc, ...val.pipeline.data_sources];
                        return acc;
                    }, []);
                });
            });
        }

        get({ id, status }) {
            const { Client } = models;

            return Client.findOne({
                where: {
                    id,
                },
                include: [{all: true, nested: true}]
            });
        }
    }

    return new Clients();
}
