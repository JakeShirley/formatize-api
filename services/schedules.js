module.exports = function (models, services, { s3, sqs }) {
    class Schedules {
        constructor() {
            this.key = "schedules";
        }

        async getLastTimePeriod({ scheduleId }) {
            const { TimePeriod } = models;
            const timePeriod = await TimePeriod.findOne({ 
                where: {
                    schedule_id: scheduleId,
                },
                order: [ [ 'start', 'ASC' ]],
            });

            return timePeriod;
        }

        async createTimePeriods({ start, end, clientId, scheduleId }) {
            const { Schedule, TimePeriod } = models;
            const schedule = await Schedule.findOne({ 
                where: {
                    id: scheduleId
                }
            });

            const ms = schedule.duration * 24 * 3600 * 1000;
            const today = new Date();

            start = start || new Date(schedule.start.getTime());
            end = end || (schedule.end ? new Date(schedule.end.getTime()) : today);

            let creations = schedule.recurring ? [] : [{
                start: start,
                end: end,
                schedule_id: schedule.id,
                client_id: clientId
            }];

            if (schedule.recurring && schedule.repeat_period) {
                while (true) {
                    let diff = schedule.repeat_every;

                    start = new Date(start.getTime());

                    switch (schedule.repeat_period) {
                        case "day": {
                            start.setDate(start.getDate() + schedule.repeat_every);
                            break;
                        }
                        case "week": {
                            start.setDate(start.getDate() + (schedule.repeat_every * 7));
                            break;
                        }
                        case "month": {
                            start.setMonth(start.getMonth() + schedule.repeat_every);
                            break;
                        }
                        case "year": {
                            start.setFullYear(start.getFullYear() + schedule.repeat_every);
                            break;
                        }
                    }

                    if (start >= end)
                        break;

                    let newEnd = new Date(start.getTime());
                    newEnd.setDate(newEnd.getDate() + schedule.duration);

                    if (newEnd > end)
                        newEnd = end;

                    creations.push({
                        start,
                        end: newEnd,
                        schedule_id: schedule.id,
                        client_id: clientId,
                    });
                }
            }

            return await TimePeriod.bulkCreate(creations);
        }

        get({ id }) {
            const { Schedule } = models;

            return Schedule.findOne({
                where: {
                    id,
                },
                include: [{all: true, nested: true}]
            });
        }
    }

    return new Schedules();
}
