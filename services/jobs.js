const QUEUE_URL = process.env.QUEUE_URL;

module.exports = function (models, services, { s3, sqs }) {
    class Jobs {
        constructor() {
            this.key = "jobs";
        }

        getGroup({ id }) {
            const { Job, JobGroup, Pipeline } = models;

            return JobGroup.findOne({
                where: {
                    id
                },
                include: [{
                    model: Job,
                    as: "jobs",
                    where: {
                        valid: true
                    }
                }]
            });  
        }

        async getOrCreateGroup({ outputName, jobCount, status, valid }) {
            const { JobGroup } = models;

            let found = await JobGroup.findOne({
                where: {
                    output_name: outputName
                }
            });

            if (found)
                return found;

            return JobGroup.create({
                output_name: outputName,
                job_count: jobCount,
                status: status || "NOT_STARTED"
            });
        }

        async createGroup({ outputName, jobCount, status, valid }) {
            const { JobGroup } = models;

            return JobGroup.create({
                output_name: outputName,
                job_count: jobCount,
                status: status || "NOT_STARTED"
            });
        }

        async makeJobsWithFilenameInvalid({ filename }) {
            const { Job } = models;

            return Job.update({
                valid: false
            },
            {
                where: {
                    filename,
                    valid: true
                }
            });
        }

        async create({ startStage, eTag, mapping, filename, group }) {
            const { Job, TransformMapping, TransformMappingStage, JobPart, JobStage } = models;

            //let ETag = eTag || (await s3.headObject({ Key: filename }).promise()).ETag;

            let preview = typeof group === "undefined";

            let job = await Job.create({
                filename: filename,
                job_group_id: group,
                mapping_id: mapping,
                valid: true,
                preview,
                //aws_etag: ETag,
                status: "NOT_STARTED"
            });

            const params = {
                DelaySeconds: 1,
                MessageBody: JSON.stringify({
                    jobId: job.id,
                }),
                QueueUrl: process.env.QUEUE_URL
            };

            try {
                let res = await sqs.sendMessage(params).promise();
            } catch (e) {
                console.log("Error sending sqs message", e);
            }

            const { id, status, started_by, output_filename } = job;
            return { id, status, started_by, output_filename };
        }

        get({ id, status }) {
            const { Job } = models;

            return Job.findOne({
                where: {
                    id,
                    status
                },
                include: [{all: true, nested: true}]
            });
        }
    }

    return new Jobs();
}
