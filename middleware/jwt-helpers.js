const ADMIN_GROUP_NAME = "Administrators";

function adminOnly(req, res, next) {
    const { groups } = req.user;

    if (groups.includes(ADMIN_GROUP_NAME)) {
        next();
    } else {
        res.status(401).send("You are not an admin.").end();
    }
}

function claimMatchesParam(claim, param) {
    return (req, res, next) => {
        const { groups } = req.user;

        if (groups.includes(ADMIN_GROUP_NAME) || req.user[claim] === req.params[param]) {
            next();
        } else {
            res.status(401).send("You are not an admin.").end();
        }
    }
}

module.exports = { adminOnly, claimMatchesParam };
