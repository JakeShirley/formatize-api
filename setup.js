require('dotenv').config();

const express = require('express');
const app = express();

const getPort = require("get-port");
const passport = require("passport");

const oktaAuth = require("./passport/okta.js");
const apiKeyAuth = require("./passport/api-key-auth.js");
const mockAuth = require("./passport/mock-auth.js");

const cors = require("cors");
const bodyParser = require('body-parser');

const session = require('express-session');

const models = require("./models");

const AWS = require('aws-sdk/global');
const S3 = require('aws-sdk/clients/s3');
const SQS = require('aws-sdk/clients/sqs');

const controllers = require("./controllers");

AWS.config.httpOptions.timeout = 0;
AWS.config.region = 'eu-west-1';

const BUCKET_NAME = process.env.BUCKET_NAME;

let s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: BUCKET_NAME }
});

oktaAuth(passport, models);
apiKeyAuth(passport, models);

if (process.env.NODE_ENV == "test") {
    mockAuth(passport, models);
}

let sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

app.use(cors());
app.use(express.json({ limit: "50mb" }));

if (process.env.NODE_ENV != "worker") {
    app.use(session({
      secret: 'Password123',
      resave: false,
      saveUninitialized: true
    }));

    app.use(passport.initialize());
    app.use(passport.session());
}

if (process.env.NODE_ENV !== "worker") {
    let authList = ["okta-jwt", "api-key"];
    if (process.env.NODE_ENV === "test") {
        authList = ["mock-auth", ...authList];
    }
    
    app.use(passport.authenticate(authList, { session: true }));
}

app.use("/static", express.static("public/build/"));

const services = require("./services")(models, { s3, sqs });

services.worker.setMaximumJobs(process.env.MAXIMUM_JOBS || 1);

var i = null;

controllers(app, models, services, { s3, sqs });

if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
    console.log("Starting development worker...");
    let params = {
        AttributeNames: [
            "SentTimestamp"
        ],
        MaxNumberOfMessages: 10,
        MessageAttributeNames: [
            "All"
        ],
        QueueUrl: process.env.QUEUE_URL,
        VisibilityTimeout: 20,
        WaitTimeSeconds: 0
    };

    i = setInterval(async () => {
        let data = await sqs.receiveMessage(params).promise();
        if (data.Messages) {
            let toBeDeleted = [];
            for (var msg of data.Messages) {
                let processed = await services.worker.process(JSON.parse(msg.Body));

                if (processed) {
                    toBeDeleted.push({Id: msg.MessageId, ReceiptHandle: msg.ReceiptHandle});
                }

                if (toBeDeleted.length > 0) {
                    await sqs.deleteMessageBatch({ Entries: toBeDeleted, QueueUrl: process.env.QUEUE_URL }).promise();
                }
            }
        }
    }, 1000);
}

console.log("Server started");

module.exports = async port => {
    let res = app.listen(process.env.NODE_ENV === "test" ? await getPort() : port || 80);
    let origClose = res.close;
    res.close = async e => {
        if (i !== null) {
            clearInterval(i);
        }

        await models.sequelize.close();
        await services.worker.stop();

        return await origClose.bind(res).call();
    }
    return res;
}
module.exports.app = app;
