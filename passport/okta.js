var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const jwksRsa = require('jwks-rsa');

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKeyProvider = jwksRsa.passportJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: process.env.OKTA_ISSUER + '/v1/keys'
  });
opts.issuer = process.env.OKTA_ISSUER;
opts.audience = process.env.OKTA_AUD;

module.exports = passport => {
    passport.use("okta-jwt", new JwtStrategy(opts, function(jwt_payload, done) {
        done(null, jwt_payload);
    }));
}
