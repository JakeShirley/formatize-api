const HeaderAPIKeyStrategy = require("passport-headerapikey").HeaderAPIKeyStrategy;
const fetch = require("node-fetch");

const OKTA_TOKEN = "001u4AFHQzFMmw47aVSAxBnSSIW-6J-WyiHy71F0f-";
const OKTA_END_API = "https://dev-331963.okta.com/api/v1";


module.exports = (passport, models) => {
    passport.use("api-key", new HeaderAPIKeyStrategy(
        {
            header: 'Authorization',
            prefix: 'API-Key ' 
        },
        false,
        function(apikey, done) {
          models.ClientAPIKey.findOne({
              where: {
                  api_key: apikey
              }
          }).then(res => {
              if (!res) {
                  done();
                  return;
              }
              Promise.all([fetch(OKTA_END_API + "/users/" + res.user_id, {
                  method: "GET",
                  headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                      Authorization: "SSWS " + OKTA_TOKEN
                  }
              }).then(res => res.json()),
              fetch(OKTA_END_API + "/users/" + res.user_id + "/groups", {
                  method: "GET",
                  headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                      Authorization: "SSWS " + OKTA_TOKEN
                  }
              }).then(res => res.json())]).then(([user, groups]) => {
                  const x = {...user.profile, groups: groups.map(x => x.profile.name)};
                  done(null, x);
              })
          }).catch(err => {
              console.log(err);
          })
        }
      ));
}