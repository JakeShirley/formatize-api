const MockStrategy = require('passport-mock-strategy');

module.exports = (passport, models) => {
    passport.use("mock-auth", new MockStrategy({
        name: 'my-mock',
        user: {
            id: "Test",
            sub: "test-user@formatize.io",
            uid: "UID_TEST",
            groups: ["Administrators"]
        }
    }));
}