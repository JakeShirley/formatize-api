module.exports = {
  "development": {
    "username": "postgres",
    "password": "password123",
    "database": "svat_aggregator",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "operatorsAliases": false,
    "logging": false
  },
  "test": {
      "username": "postgres",
      "password": "password123",
      "database": "svat_aggregator",
      "host": "127.0.0.1",
      "dialect": "postgres",
      "operatorsAliases": false,
      "logging": false
  },
  "production": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "dialect": process.env.DB_DIALECT,
    "operatorsAliases": false,
    "logging": false
},
"worker": {
  "username": process.env.DB_USERNAME,
  "password": process.env.DB_PASSWORD,
  "database": process.env.DB_NAME,
  "host": process.env.DB_HOST,
  "dialect": process.env.DB_DIALECT,
  "logging": false,
  "operatorsAliases": false
}
}
