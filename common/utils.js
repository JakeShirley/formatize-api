const csv = require("csv-parser");

async function S3ReadLinesInFile(s3, bucket, key, lineCount) {
    const BYTES = 1024 * 5;

    let lines = [];
    let curByte = BYTES;
    let content = "";

    while (lines.length < lineCount + 1) {
        try {
            lines = await new Promise((good, bad) => {
                let lines = [];

                let exec = s3.getObject({
                    Bucket: bucket,
                    Key: key,
                    Range: "bytes=0-" + (curByte)
                }, function (err, res) {
                    if (res.ContentLength <= curByte) {
                        bad(lines);
                    } else {
                        good(lines);
                    }
                }).createReadStream()
                .pipe(csv(key.endsWith(".txt") ? { separator: "\t" } : {}))
                .on('data', row => lines.push(row));
            });
        } catch (e) {
            if (lines.length) {
                lines = e;
            } else {
                throw e;
            }
            break;
        }

        curByte += BYTES;
    }

    if (lines.length > lineCount)
        lines.length = lineCount;

    return lines;
}

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

module.exports = { S3ReadLinesInFile, dateDiffInDays };
