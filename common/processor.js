const parse = require('csv-parse');
const parseSync = require('csv-parse/lib/sync')
const exceljs = require("exceljs");

class Reader {
    constructor(s3) {
        this.s3 = s3;
    }

    async getPartByteRanges(key, mb = 24) {}

    async getHeaders(key) {}

    read(key, startRange, endRange, cb) {}
}

class XLSXReader extends Reader {
    constructor(s3) {
        super(s3);
    }

    async getPartByteRanges(key) {    
        let fileSize = await this.s3.headObject({ Key: key })
            .promise()
            .then(res => res.ContentLength);

        return [{
            start: 0,
            end: fileSize
        }]
    }

    getHeaders(key) {
        return new Promise((good, bad) => {
            let stream = this.s3.getObject({Key: key}).createReadStream();

            var workbook = new exceljs.stream.xlsx.WorkbookReader();
            let headers = [];

            workbook.on('worksheet', function (worksheet) {          
                worksheet.on('row', function (row) {
                    headers = row.model.cells.map(x => x.value);
                    console.log(row.model);
                    good(headers);
                });
            });

            workbook.read(stream, {
                entries: "emit",
                sharedStrings: "cache",
                worksheets: "emit"
            });
        })
    }

    read(key, startRange, endRange, cb) {
        return new Promise(async (good, bad) => {
            let stream = this.s3.getObject({Key: key}).createReadStream();

            var workbook = new exceljs.Workbook();//new exceljs.stream.xlsx.WorkbookReader();
            let headers = [];
            let first = true;

            await workbook.xlsx.read(stream);

            var worksheet = workbook.getWorksheet(1);
            let rows = worksheet.model.rows;

            for (let i = 1; i < rows.length; i++) {
                let r = rows[i];
                cb(r.cells.map(x => x.value instanceof Date ? x.value.getTime() : x.value));
            }

            good(worksheet.model.rows);

            /*workbook.on('worksheet', function (worksheet) {          
                worksheet.on('row', function (row) {
                    if (first) {
                        first = false;
                    } else {
                        let vals = row.model.cells.map(x => x.value);
                        console.log(vals);
                        cb(vals);
                    }
                });
            });

            workbook.on('finished', function () {
                good();
            });
        
            workbook.on('close', function () {
                good();
            });

            workbook.read(stream, {
                entries: "emit",
                sharedStrings: "cache",
                worksheets: "emit"
            });*/
        })
    }
}

class CSVReader extends Reader {
    constructor(s3) {
        super(s3);
    }

    async getPartByteRanges(key, mb = 20) {
        const LINE_SIZE = 1024 * 10;
        const BLOCK_SIZE = 1024 * 1024 * mb;
    
        let parts = [];
        let start = 0;
        let end = 0;
    
        let fileSize = await this.s3.headObject({ Key: key })
            .promise()
            .then(res => res.ContentLength);
    
        while (true) {
            end += BLOCK_SIZE;
    
            if (end >= fileSize) {
                parts.push({ start, end: fileSize });
                return parts;
            } else {
                let lines = [];
    
                let lineSize = LINE_SIZE;
                while (lines.length <= 1) {
                    if (end + lineSize >= fileSize) {
                        parts.push({start, end: fileSize });
                        return parts;
                    } else {
                        let data = await this.s3.getObject({ Key: key,
                            Range: 'bytes=' + end + "-" + (end + lineSize)}).promise();
    
                        let csv = data.Body.toString();
                        lines = csv.match(/[^\r\n]+/g);
                        lineSize += LINE_SIZE;
                    }
                }
    
                let lineLen = Buffer.byteLength(lines[0], 'utf8');
    
                end += lineLen;
    
                parts.push({start, end });
    
                start = end + 1;
    
                if (end >= fileSize)
                    break;
            }
        }
    
        return parts;
    }

    async getHeaders(key) {
        let size = (await this.s3.headObject({Key: key}).promise()).ContentLength;

        return new Promise(async (good, bad) => {
            const BLOCK_SIZE = 1024;
            let end = Math.min(BLOCK_SIZE, size);
            let results = [];

            while (end <= size) {
                results = await this.s3.getObject({ Key: key, Range: "bytes=0-" + end }).promise().then(data => {
                    return parseSync(data.Body.toString(), {
                        relax_column_count: true,
                        delimiter: key.endsWith(".txt") ? "\t" : ","
                    });
                });

                if (results.length > 1 || end == size)
                    break;

                end += BLOCK_SIZE;
                end = Math.min(end, size);
            }

            good(results[0]);
        });
    }

    //function s3Processor(s3, key, startRange, endRange, headers, transforms, clone) {
    read(key, startRange, endRange, cb) {
        return new Promise((good, bad) => {    
            this.s3.getObject({ Key: key, Range: "bytes=" + startRange + "-" + endRange }, function(err, data) {
                if (err) {
                    bad(err);
                } else {
                    const parser = parse({
                        delimiter: key.endsWith(".txt") ? "\t" : ",",
                        relax: true,
                        relax_column_count: true
                    });
                    let first = false;
                    parser.on('readable', function(){
                        if (!first) {
                            first = true;
                            parser.read();
                        }
                        let record;
                        while (record = parser.read()) {
                            //processRow(transforms, results, clone)(record);
                            cb(record);
                        }
                    });
                    parser.on('end', function() {
                        good();
                    });
    
                    parser.write(data.Body.toString());
                    parser.end();
                }
            })
        });
    }
}

function processRow(transforms, output, clone) {
    return data => {
        let row = clone ? { ...data } : {};

        for (var trans of transforms) {
            trans(data, row);
        }

        output.push(row);
    }
}

function outputToString(output, joiner = ",") {
    return output.map(x => {
        return Object.keys(x).map(y => x[y]).join(joiner)
    }).join("\r\n");
}

module.exports = { CSVReader, XLSXReader, processRow, outputToString };
