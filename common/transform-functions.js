function keepColumn({ column }, { oHeaders, headers, output }) {
    let ref = "REF_" + column;
    headers[ref] = oHeaders[column];

    return (row, acc) => {
        acc[ref] = row[column];
    }
}

function renameColumn({src, dest}, { headers, clone }) {
    headers["REF_" + src] = dest;

    return (row, acc) => {
    }
}

function addColumn({ column, location }, { oHeaders, headers, callCount }) {
    const ref = "REF_" + (oHeaders.length - 1 + callCount);
    headers[ref] = column;

    return (row, acc) => {
        acc[ref] = "";
    }
}

function cloneColumn({src, dest}, { headers, clone }) {
    return (row, acc) => {
        acc["REF_" + dest] = row[src];
    }
}

function fillColumn({column, str}, { headers, clone }) {
    return (row, acc) => {
        acc["REF_" + column] = str;
    }
}

module.exports = {
    "rename-column": {
        label: "Rename column",
        params: {
            src: {
                type: "column",
                label: "Source column",
                src: true,
                inAccumulator: true
            },
            dest: {
                type: "text",
                label: "New name"
            }
        },
        func: renameColumn
    },
    "fill-column": {
        label: "Fill column",
        params: {
            column: {
                type: "column",
                label: "Column",
                src: true,
                inAccumulator: true
            },
            str: {
                type: "text",
                label: "Text"
            }
        },
        func: fillColumn
    },
    "add-column": {
        label: "Add column",
        params: {
            column: {
                type: "text",
                label: "Name of the column"
            }
        },
        func: addColumn
    },
    "clone-column": {
        label: "Clone column",
        params: {
            src: {
                type: "column",
                label: "Source column",
                src: true
            },
            dest: {
                type: "column",
                label: "Destination column",
                inAccumulator: true
            }
        },
        func: cloneColumn
    },
    "keep-column": {
        label: "Keep column",
        params: {
            column: {
                type: "column",
                label: "Column to keep",
                src: true,
            }
        },
        func: keepColumn
    },
    "replace-string": {
        label: "Replace string",
        params: {
            column: {
                type: "column",
                label: "Column",
                src: true,
                inAccumulator: true
            },
            isRegex: {
                type: "boolean",
                label: "Regex?",
            },
            string: {
                type: "text",
                label: "String",
            },
            replacement: {
                type: "text",
                label: "Replacement",
            }
        },
        func: ({column, isRegex, string, replacement = ""}) => {
            let replacer = isRegex ? new RegExp(string) : string;
            return (row, acc) => {
                acc["REF_" + column] = acc["REF_" + column].replace(replacer, replacement);
            }
        }
    }
};
