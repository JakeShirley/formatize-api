var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var Transform = sequelize.define('Transform', {
        name: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        label: Sequelize.STRING,
        order: Sequelize.INTEGER,
        params_json: {
          type: Sequelize.TEXT,
          defaultValue: ""
        },
        func: Sequelize.TEXT,
        locked: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    Transform.associate = function(models) {
    }

    return Transform;
}
