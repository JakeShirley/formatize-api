var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var TransformInstance = sequelize.define('TransformInstance', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        mapping_stage_id: Sequelize.INTEGER,
        name: Sequelize.STRING,
        order: Sequelize.INTEGER,
        params_json: Sequelize.STRING
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    TransformInstance.associate = function(models) {
        TransformInstance.belongsTo(models.TransformMappingStage, {as: "owner", foreignKey: "mapping_stage_id"});
        TransformInstance.belongsTo(models.Transform, {as: "transform", foreignKey: "name"});
    }

    return TransformInstance;
}
