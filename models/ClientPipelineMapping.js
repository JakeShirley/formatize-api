var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var ClientPipelineMapping = sequelize.define('ClientPipelineMapping', {
        client_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        pipeline_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        schedule_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    ClientPipelineMapping.associate = function(models) {
        ClientPipelineMapping.belongsTo(models.Pipeline, { as: "pipeline", foreignKey: "pipeline_id"});
        ClientPipelineMapping.belongsTo(models.Schedule, { as: "schedule", foreignKey: "schedule_id"});
    }

    return ClientPipelineMapping;
}
