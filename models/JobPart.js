var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var JobPart = sequelize.define('JobPart', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        job_stage_id: Sequelize.INTEGER,
        status: Sequelize.STRING,
        part: Sequelize.INTEGER,
        aws_etag: Sequelize.STRING,
        start_byte: Sequelize.BIGINT,
        end_byte: Sequelize.BIGINT,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    JobPart.associate = function(models) {
        JobPart.belongsTo(models.JobStage, { as: "job_stage", foreignKey: "job_stage_id"});
    }

    return JobPart;
}
