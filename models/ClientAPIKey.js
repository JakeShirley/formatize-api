'use strict';

var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var ClientAPIKey = sequelize.define('ClientAPIKey', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        api_key: Sequelize.STRING,
        user_id: Sequelize.STRING,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    ClientAPIKey.associate = function(models) {
    }

    return ClientAPIKey;
}
