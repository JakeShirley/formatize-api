var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var TransformMappingStage = sequelize.define('TransformMappingStage', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        mapping_id: {
            type: Sequelize.STRING,
            references: {
                model: 'TransformMapping',
                key: 'id'
            },
            onDelete: 'CASCADE'
        },
        name: Sequelize.STRING,
        description: Sequelize.STRING,
        clone_data: Sequelize.BOOLEAN,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    TransformMappingStage.associate = function(models) {
        TransformMappingStage.belongsTo(models.TransformMapping, { as: "mapping", foreignKey: "mapping_id" });
        TransformMappingStage.hasMany(models.TransformInstance, { as: "transforms", foreignKey: "mapping_stage_id" });
    }

    return TransformMappingStage;
}
