var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var TransformMapping = sequelize.define('TransformMapping', {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        data_validator_id: Sequelize.INTEGER,
        name: Sequelize.STRING,
        description: Sequelize.TEXT,
        signature: Sequelize.TEXT,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    TransformMapping.associate = function(models) {
        TransformMapping.hasMany(models.TransformMappingStage, { as: "stages", foreignKey: "mapping_id" });
        TransformMapping.belongsTo(models.DataValidator, { as: "validator", foreignKey: "data_validator_id" });
    }

    return TransformMapping;
}
