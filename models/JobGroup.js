var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var JobGroup = sequelize.define('JobGroup', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        job_count: Sequelize.INTEGER,
        output_name: Sequelize.STRING,
        status: Sequelize.STRING,
        client_id: Sequelize.INTEGER,
        time_period_id: Sequelize.INTEGER
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    JobGroup.associate = function(models) {
        JobGroup.hasMany(models.Job, { as: "jobs", foreignKey: "job_group_id"});
        //JobGroup.belongsTo(models.ClientPipelineMapping, { as: "client_pipeline", foreignKey: "client_pipeline_mapping_id"});
    }

    return JobGroup;
}
