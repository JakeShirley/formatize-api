var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var Pipeline = sequelize.define('Pipeline', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: Sequelize.STRING
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    Pipeline.associate = function(models) {
        Pipeline.hasMany(models.DataSource, { as: "data_sources", foreignKey: "pipeline_id"});
        Pipeline.hasMany(models.PipelineMapping, { as: "mapping_list", foreignKey: "pipeline_id"});
    }

    return Pipeline;
}
