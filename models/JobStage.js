var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var JobStage = sequelize.define('JobStage', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        job_id: Sequelize.INTEGER,
        status: Sequelize.STRING,
        mapping_stage_id: Sequelize.INTEGER,
        stage: Sequelize.INTEGER,
        filename: Sequelize.STRING,
        output_filename: Sequelize.STRING,
        aws_upload_id: Sequelize.STRING,
        aws_etag: Sequelize.STRING,
        started_at: Sequelize.DATE,
        finished_at: Sequelize.DATE,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    JobStage.associate = function(models) {
        JobStage.belongsTo(models.Job, { as: "job", foreignKey: "job_id" });
        JobStage.belongsTo(models.TransformMappingStage, { as: "mapping_stage", foreignKey: "mapping_stage_id" });
        JobStage.hasMany(models.JobPart, { as: "parts", foreignKey: "job_stage_id" });
    }

    return JobStage;
}
