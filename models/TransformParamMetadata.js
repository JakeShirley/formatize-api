var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var TransformParamMetadata = sequelize.define('TransformParamMetadata', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        transform_name: Sequelize.STRING,
        transform_instance_id: Sequelize.INTEGER,
        column_header: Sequelize.STRING,
        type: Sequelize.STRING,
        value: Sequelize.TEXT
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    TransformParamMetadata.associate = function(models) {
        TransformParamMetadata.belongsTo(models.Transform, { as: "transform", foreignKey: "transform_name" });
        TransformParamMetadata.belongsTo(models.TransformInstance, { as: "transform_instance", foreignKey: "transform_instance_id" });
    }

    return TransformParamMetadata;
}
