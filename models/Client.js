var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var Client = sequelize.define('Client', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: Sequelize.STRING,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    Client.associate = function(models) {
        Client.hasMany(models.ClientPipelineMapping, { as: "pipeline_list", foreignKey: "client_id"});
        Client.hasMany(models.TimePeriod, { as: "time_periods", foreignKey: "client_id"});
    }

    return Client;
}
