var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var DataSource = sequelize.define('DataSource', {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        pipeline_id: {
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        type: Sequelize.STRING,
        name: Sequelize.STRING,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    DataSource.associate = function(models) {
        DataSource.belongsTo(models.Pipeline, { as: "pipeline", foreignKey: "pipeline_id"});
    }

    return DataSource;
}
