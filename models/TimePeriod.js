var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var TimePeriod = sequelize.define('TimePeriod', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        schedule_id: Sequelize.INTEGER,
        client_id: Sequelize.INTEGER,
        start: Sequelize.DATEONLY,
        end: Sequelize.DATEONLY
    }, {
        freezeTableName: true
    });

    TimePeriod.associate = function(models) {
        TimePeriod.belongsTo(models.Schedule, { as: "schedule", foreignKey: "schedule_id" });
    }

    return TimePeriod;
}
