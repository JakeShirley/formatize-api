var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var Job = sequelize.define('Job', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        aws_etag: Sequelize.STRING,
        status: Sequelize.STRING,
        started_by: Sequelize.STRING,
        started_at: Sequelize.DATE,
        finished_at: Sequelize.DATE,
        filename: Sequelize.STRING,
        output_filename: Sequelize.STRING,
        mapping_id: Sequelize.STRING,
        job_group_id: Sequelize.INTEGER,
        preview: Sequelize.BOOLEAN,
        valid: Sequelize.BOOLEAN,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    Job.associate = function(models) {
        Job.belongsTo(models.TransformMapping, { as: "mapping", foreignKey: "mapping_id"});
        Job.hasMany(models.JobStage, { as: "stages", foreignKey: "job_id"})
    }

    return Job;
}
