var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var Schedule = sequelize.define('Schedule', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: Sequelize.STRING,
        start: {
            type: Sequelize.DATE,
            allowNull: false
        },
        end: {
            type: Sequelize.DATE,
        },
        duration: Sequelize.INTEGER,
        recurring: Sequelize.BOOLEAN,
        repeat_every: Sequelize.INTEGER,
        repeat_period: Sequelize.STRING,
        backdated: Sequelize.BOOLEAN,
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    Schedule.associate = function(models) {
    }

    return Schedule;
}
