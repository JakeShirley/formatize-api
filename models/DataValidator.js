'use strict';

var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var DataValidator = sequelize.define('DataValidator', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        name: Sequelize.STRING,
        description: Sequelize.TEXT,
        columns: Sequelize.ARRAY(Sequelize.STRING),
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    DataValidator.associate = function(models) {
    }

    return DataValidator;
}
