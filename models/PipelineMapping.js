var Sequelize = require("sequelize");

module.exports = function (sequelize) {
    var PipelineMapping = sequelize.define('PipelineMapping', {
        pipeline_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        mapping_id: {
            type: Sequelize.STRING,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
        data_source_id: {
            type: Sequelize.STRING,
            primaryKey: true,
            onDelete: 'CASCADE'
        },
    }, {
        freezeTableName: true // Model tableName will be the same as the model name
    });

    PipelineMapping.associate = function(models) {
        PipelineMapping.belongsTo(models.TransformMapping, { as: "mapping", foreignKey: "mapping_id"});
        PipelineMapping.belongsTo(models.DataSource, { as: "data_source", foreignKey: "data_source_id"});
    }

    return PipelineMapping;
}
